import { UserSession } from 'blockstack'
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
const { encryptECIES } = require('blockstack/lib/encryption')
const STEALTHY_PK = 'pk.txt'
const GRAPHITE_PK = 'key.json'
const XORDRIVE_PK = 'index.json'
const GRAPHITE_EXPORT_INDEX_FILE = 'GraphiteExportIndex.json'
const STEALTHY_EXPORT_INDEX_FILE = 'StealthyExportIndex.json'
const XORDRIVE_EXPORT_INDEX_FILE = 'XORDriveExportIndex.json'

// const blockstack = window.blockstack
const userSession = new UserSession()

// Load the public key for Graphite (note: I'm using the stealthy example here)
export function loadKey (site) {
  if (this.$store.state.username === '') {
    // error
    return -1
  }
  let pkFile = ''
  if (site === 'graphite') {
    UserSession.getUserAppFileUrl(
      GRAPHITE_PK,
      this.$store.state.username,
      'https://app.graphitedocs.com'
    )
      .then(response => {
        graphiteUrl = response.body
        console.log('graphite pk url:')
        console.log(response)
        console.log('username:' + this.$store.state.username)
        // call loadkey
        let pk = this.loadKey(graphiteUrl)
        // encrypt with public key
        this.fileName = 'graphiteIndex.json'
        this.encryptData(pk)
        // write file
        this.exportEncryptedData()
        // reset component data
      })
      .catch(err => {
        console.log(err)
      })
  } else if (site === 'xordrive') {
    pkFile === XORDRIVE_PK
  }
  axios
    .get(url + pkFile) // target app gaia hub url and public key
    .then(response => {
      this.setState({
        stealthyKey: response.data,
        stealthyConnected: true
      })
    })
    .then(() => {
      this.loadSharedDocs()
    })
    .catch(error => {
      console.log('error:', error)
    })
}

// ## Then load the files to be sent to the other app
export function loadSharedDocs () {
  userSession
    .getFile('documentscollection.json', { decrypt: true })
    .then(fileContents => {
      if (JSON.parse(fileContents || '{}').value) {
        this.setState({
          docs: JSON.parse(fileContents || '{}').value
        })
      } else {
        console.log('No saved files')
      }
    })
    .then(() => {
      this.saveStealthyIntegration()
    })
    .catch(error => {
      console.log(error)
    })
}

// ## Finally, save the collection with the receiving app's public key
export function saveStealthyIntegration () {
  const data = this.state.docs
  const publicKey = this.state.stealthyKey
  const encryptedData = JSON.stringify(
    encryptECIES(publicKey, JSON.stringify(data))
  )
  const fileName = 'stealthyIndex.json'
  putFile(fileName, encryptedData, { encrypt: false })
    .then(() => {
      if (window.location.pathname === '/integrations') {
        window.Materialize.toast('Stealthy integration updated', 4000)
        this.saveIntegrations()
      }
    })
    .catch(e => {
      console.log(e)
    })
}
