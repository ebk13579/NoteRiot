// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import '@mdi/font/css/materialdesignicons.min.css'
import App from './App'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import store from './store'
import router from './router'
import colors from 'vuetify/es5/util/colors'
// import '@universelabs/meta/scss/meta.scss'
// import './stylus/main.styl'

Vue.use(Vuetify, {
  theme: {
    // primary: '#9c27b0', // material violet-5 meta-ui palette
    // primary: '#582f6b',
    // -- primary: colors.purple.darken2,
    primary: '#77216F',
    // primary: '#654879',
    // primary: '#582F6B',
    // primary: '#493D8A',
    secondary: '#D6BCD3', // colors.purple.lighten3,
    // secondary: '#ffffff', // meta-ui fuschia-5
    // accent: colors.teal.accent3, // colors.teal.accent3,
    accent: colors.orange,
    // accent: '#009688', // material teal-5 palette
    error: colors.red.darken2, // colors.red.accent1,
    warning: colors.yellow.darken2, // colors.yellow.accent1,
    info: colors.blue.darken2, // colors.blue.accent1,
    success: colors.green.darken2 // colors.green.accent1
  },
  iconfont: 'mdi'
})

Vue.config.productionTip = false

window.blockstack = require('blockstack')

Vue.prototype.$eventHub = new Vue()

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
